package com.devcamp.s10.employeerestapi;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeControl {

    @GetMapping("/employees")
    public ArrayList<Employee> getEmployees(){

        //  khởi tạo 3 đối tượng sinh viên 

        Employee nhanVien1 = new Employee(10226, "thuong", "nguyen", 5000);
        Employee nhanVien2 = new Employee(10228, "huy", "dao", 6000);
        Employee nhanVien3 = new Employee(10229, "vinh", "le", 5000);
        // in thông tin của ba nhân viên
        System.out.println(nhanVien1);
        System.out.println(nhanVien2);
        System.out.println(nhanVien3);
        // thêm ba nhân viên vào 1 danh sách

        ArrayList<Employee> employees = new ArrayList<Employee>();
        employees.add(nhanVien1);
        employees.add(nhanVien2);
        employees.add(nhanVien3);
        return employees;
    }
    
}
