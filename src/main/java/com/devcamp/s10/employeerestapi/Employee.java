package com.devcamp.s10.employeerestapi;




public class Employee {

   
    
    private  int id;
    private String firstName;
    private String lasttName;
    private  int salary;
    public Employee(int id, String firstName, String lasttName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lasttName = lasttName;
        this.salary = salary;
    }
    public int getId() {
        return id;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLasttName() {
        return lasttName;
    }
    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary ;
    }

    public int getAnnualSalary() {
        return salary * 12 ;
    }

    public int raiseSalary(int percent) {
        return salary * (1+ percent / 100) ;  // trả về mức lương tương ứng
    }
    @Override
    public String toString() {
        return "Employee [id=" + id + ", firstName=" + firstName + ", lasttName=" + lasttName + ", salary=" + salary
                + "]";
    }
    

    //  phương thức khởi tạo 


   
    
    
}
