package com.devcamp.s10.employeerestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeerestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeerestApiApplication.class, args);
	}

}
